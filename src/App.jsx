import { useState } from "react";
import "./App.css";
import MainContainer from "./components/MainContainer";

function App() {
  const listOfItems = [
    {
      name: "item1",
      id: 1,
      quantity: 0
    },
    {
      name: "item2",
      id: 2,
      quantity: 0
    },
    {
      name: "item3",
      id: 3,
      quantity: 0
    },
    {
      name: "item4",
      id: 4,
      quantity: 0
    }
  ];
  return <MainContainer itemList={listOfItems}/>;
}

export default App;
