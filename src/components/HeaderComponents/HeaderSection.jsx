import React,{useState}from "react";
import ItemCounter from "./ItemCounter";
import "./HeaderSection.css";

const HeaderSection = ({items}) => {


  return (
    <>
      <div className="header-section">
        <i className="fa-solid fa-cart-shopping fa-lg" style={{ color: "#000" }}></i>

        <ItemCounter itemCount={items} />
        <div>Items</div>
      </div>
    </>
  );
};

export default HeaderSection;
