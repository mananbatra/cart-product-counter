import React from 'react';
import './ItemCounter.css'

const ItemCounter = ({itemCount}) => {
  return (
        <div className='itemCounter'>{itemCount}</div>
  )
}

export default ItemCounter