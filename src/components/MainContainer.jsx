import React, { useState } from "react";
import HeaderSection from "./HeaderComponents/HeaderSection";
import RefreshCartButtons from "./RefreshCart/RefreshCartButtons";
import CartItems from "./CartItems/CartItems";
import "./MainContainer.css";

const MainContainer = ({ itemList }) => {
  const [newItems, setNewItems] = useState([...itemList]);

  const itemCount=newItems.reduce((acc,currentItem)=>
  {
    if(currentItem.quantity>0)
    {
      ++acc;
    }
    return acc;
  },0)

  const handleIncrement = (itemId) => {
    setNewItems((oldItems) => {
      const updatedItems = oldItems.map((item) =>
        item.id === itemId ? { ...item, quantity: item.quantity + 1 } : item
      );
      return updatedItems;
    });
  };

  const handleDecrement = (itemId) => {
    setNewItems((oldItems) => {
      const updatedItems = oldItems.map((item) =>
        item.id === itemId
          ? { ...item, quantity: item.quantity > 0 ? item.quantity - 1 : 0 }
          : item
      );
      return updatedItems;
    });
  };

  const handleDelete= (itemId) =>
  {
    setNewItems((oldItems) =>
    {
      const itemsLeftInCart = oldItems.filter((item) => item.id !== itemId)
      return itemsLeftInCart;
    })
  }

  const handleReset = () =>
  {
      setNewItems((oldItems) =>
      {
          const resetQuantity= oldItems.map((item)=> ({...item,quantity:0}));
          return resetQuantity;
      })
  }

  const handleRestoreCart =()=>
  {
      setNewItems((oldItems)=>
      {
        if(oldItems.length==0)
        {
          return [...itemList];
        }
        return oldItems;
      })
  }

  return (
    <div className="container">
      <HeaderSection items={itemCount}/>
      <RefreshCartButtons items={newItems} onReset={handleReset} onRestore={handleRestoreCart}/>
      <CartItems
        items={newItems}
        onAdd={handleIncrement}
        onReduce={handleDecrement}
        onDelete={handleDelete}
      />
    </div>
  );
};

export default MainContainer;
