import React from 'react';
import './ReduceQuantity.css'

const ReduceQuantity = ({onClick, quantity}) => {
  return (
    <button className={quantity>0?'reduce-button':'reduce-button opacity'} onClick={onClick}><i className="fa-solid fa-circle-minus" style={{color: "#ffffff"}}></i></button>
  )
}

export default ReduceQuantity