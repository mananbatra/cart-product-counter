import React from 'react';
import './AddQuantity.css';

const AddQuantity = ({Click}) => {
  return (
    <button className='add-button' onClick={Click}><i className="fa-solid fa-circle-plus" style={{color: "#ffffff"}}></i></button>
  )
}

export default AddQuantity