import React from 'react';
import './DeleteItem.css'

const DeleteItem = ({deleteClick}) => {
  return (
    <button className='delete-button' onClick={deleteClick} ><i className="fa-regular fa-trash-can" style={{color: "#ffffff"}}></i></button>
  )
}

export default DeleteItem