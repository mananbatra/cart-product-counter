import React from 'react';
import './ItemQuantity.css'

const ItemQuantity = ({quantity}) => {
  return (
    <div className={quantity>0?'blue':'yellow'}>{quantity}</div>
  )
}

export default ItemQuantity