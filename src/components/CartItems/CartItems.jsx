import React from "react";
import ItemQuantity from "./ItemQuantity";
import AddQuantity from "./AddQuantity";
import ReduceQuantity from "./ReduceQuantity";
import DeleteItem from "./DeleteItem";
import "./CartItems.css";

const CartItems = ({items, onAdd ,onReduce, onDelete}) => {

  return (
    <div className="item-list">
      {items.map((item) => (
        <div className="item-properties" key={item.id}>
          <ItemQuantity quantity={item.quantity==0?'Zero':item.quantity} />
          <AddQuantity  Click={()=>onAdd(item.id)}/>
          <ReduceQuantity onClick={()=>onReduce(item.id)} quantity={item.quantity}/>
          <DeleteItem deleteClick={()=>onDelete(item.id)}/>
        </div>
      ))}
    </div>
  );
};

export default CartItems;
