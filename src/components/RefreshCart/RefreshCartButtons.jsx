import React from "react";
import ResetQuantity from "./ResetQuantity";
import RestoreCartItems from "./RestoreCartItems";
import "./RefreshCartButtons.css";

const RefreshCartButtons = ({items,onReset,onRestore}) => {

  const totalItems=items.length;


  return (
    <div className="cart">
      <ResetQuantity handleReset={onReset} totalItems={totalItems} />
      <RestoreCartItems handleRestore={onRestore} totalItems={totalItems}/>
    </div>
  );
};

export default RefreshCartButtons;
