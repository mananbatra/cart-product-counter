import React from 'react'
import './RestoreCartItems.css'

const RestoreCartItems = ({handleRestore , totalItems}) => {
  return (
    <button className={totalItems>0?'restoreButton opacity':'restoreButton'} onClick={handleRestore}><i className="fa-solid fa-recycle" style={{color: "#ffffff"}}></i></button>
  )
}

export default RestoreCartItems