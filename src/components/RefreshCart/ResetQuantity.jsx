import React from 'react'
import './ResetQuantity.css'

const ResetQuantity = ({handleReset, totalItems}) => {
  return (
    <button className={totalItems>0?'resetButton':'resetButton opacity'} onClick={handleReset} ><i className="fa-solid fa-arrows-rotate" style={{color: "#ffffff"}}></i></button>
  )
}

export default ResetQuantity